% Chapter 1

\chapter{Introduction} % Main chapter title

\label{Chapter1} % For referencing the chapter elsewhere, use \ref{Chapter1} 

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}

%----------------------------------------------------------------------------------------
\section{Motivation}

In autonomous robot applications, an intelligent robot is expected to not only be able to perceive the environment, but also be able to interact with the environment. Among these abilities, the object grasping is the most essential and profitable in that it will bring enormous productivity to the society \cite{Sanchez2018}. Traditionally, the robot grasping is understood in term of 2 related sub-problems: perception and planning \cite{GeometryDetect2015}. The function of the perceptual component is to estimate the position and the orientation of the object to be grasped. Then the planning component computes to execute the physical grasp. One challenge is that it can be very difficult to localize an object accurately given a noisy and partial point cloud in the light clustered environment. The other considered challenge is the robustness and stability of the grasping commission.

In this paper, we mainly concentrate on the task of determining the robust grasp configuration for the perceptual ability of the autonomous robot application, which is also known as the robust grasp detection problem.

%----------------------------------------------------------------------------------------
\section{Problem constraint and statement}

Before stating the problem and formulation, we would like to present the constraints about hand kinematics and object geometry, which will affect the algorithms in next sections.

\subsection{Hand Kinematic}
For this work we deal with three-finger adaptive robot gripper \cite{Robotiq} which is a robotic peripheral that is designed for industrial applications. This robot hand has 3 articulated fingers that each has three joins.
When the hand closes the fingers, there are 2 types of grips: the finger grip happened when an object is only held by the distal phalanxes; and encompassing grip happened when the fingers surround an object.

In this work we use both 3 fingers and utilized 3 of 4 operation modes of the hand. This hypothesis is good enough for us to perform variant grasp of both 2 types of grip. The 4th operation mode is for minority cases, and it can be alternatively utilized by other modes. The three considered operation modes are: basic mode, wide mode and pinch mode. These modes are encoded into {0, 1, 2} to command the robot hand, and to be used as one parameter of grasping configuration, which will be used along next sections.

Furthermore, the maximum working aperture of the hand is equal to 200mm. The aperture range of [0, 200] mm is encoded into integer range of [0, 255] to command the open size of the gripper. This element is also considered as one parameter of grasp configuration.
The robot hand and its operation modes are illustrated in finger \ref{fig:robotiq}

%\begin{figure}
%	%	\vskip 0.2in
%	\begin{center}
%		%		\vfill
%		\subfigure[Three-finger adaptive robotic gripper]{\includegraphics[width=0.7\columnwidth]{fig/3-finger_adaptive_robot_gripper}}
%		%		\vfill
%		\subfigure[Operation modes and types of grip]{\includegraphics[width=\columnwidth]{fig/3-finger_grip_types}}
%		%		\vfill
%		\caption{Illustration of three-finger adaptive robotic gripper and its operation modes}
%		\label{fig:robotiq}
%	\end{center}
%	%	\vskip -0.2in
%\end{figure}

\subsection{Object Geometry}
In this work, we only consider he opaque, rigid objects which have at least one dimension smaller than the maximum working aperture of the robotic gripper. These objects are restricted to be opaque due to the limitations of the depth camera used with which RGBD images are acquired using projected coded IR patterns. The transparent objects would not be detected properly due to the wavelength of the emitted light that can be reflected or refracted by some materials.

%\subsection{Formulation}
%\textit{\textbf{Definition}}
\subsection{Definition}

Let $\mathcal{W} \subset \mathbb{R}^3$ denotes the viable workspace of the robot; $\mathcal{P} \subset \mathbb{R}^3$ denotes the perceived the 3D point cloud from depth sensor that is registered to the work space. 
We refer the feasible viewpoint $\mathcal{F} = \mathcal{W} \cap \mathcal{P}$ is a set of points that the sensor can sense and the robot hand can reach without any harmful.

Given a set of $n$ neutral models of $n$ objects, each neutral model $i$, $i \in {1, 2, ..n}$, includes a neutral point cloud $MP_i$ and a set of neutral grasping configurations $MC_i$.

The objective is to match the neutral grasping configurations of neutral point clouds to the corresponding object appeared in the sensed point cloud $\mathcal{F}$.

%\textit{\textbf{Problem subdivision and statement}}
\subsection{Problem subdivision and statement}

To solve this big subject, we distribute the matter into 2 sub-problems to make 2-step cascade solution:

- Given a sensed point cloud $\mathcal{F}$, distribute it into a set of point clouds: $\mathcal{S} = S_0, S_1, S_2, .., S_n$, where $S_0$ is point cloud of working space background, $S_i$ is point cloud of object $i$ in $\mathcal{F}$, $S_i$ may be $\emptyset$.

- Given an un-empty point cloud $S_i$ of object $i$ that appeared in the sensed point cloud $\mathcal{F}$, a neutral model $MP_i$ and a set of neutral grasping configurations $MC_i$. Match the neutral model to sensed point cloud $S_i$ and inference the new set of grasps onto the sensed point cloud $S:_i$.

%----------------------------------------------------------------------------------------
\section{Approach thesis overview}
We also proposed a fast and robust pipeline to deliver reliable grasp solutions. Firstly, we spent a lightweight point cloud semantic segmentation to detect and isolate the objects out of the working space. The problem of this process is that the point density of objects in cluster workspace is significantly variant due to the situation. Even the density gap between the objects may be big. These cause the network harder to learn homogeneously. Actually, the network often ignores the low density point cloud and just detect the large-point objects. Therefore we proposed to modify the network and training process to help the network learning better by applying processes of data balancing and to direct the learning. After that, we utilized the shape model and the grasp model of each object to reason the grasp. And thence we propose a criterion to evaluate the grasp candidates and deliver one final viable grasp.

%----------------------------------------------------------------------------------------
\section{Contributions}
In summarization, our main contributions are:
\begin{itemize}
	\item[--] We proposed a novel representation of the object model.
	\item[--] We proposed a grasp configuration representation specific for 3-finger robotic gripper and an end-to-end pipeline for robot grasp inference.
	\item[--] We proposed a network architecture based on PointNet that can work on imbalance data. And to improve the quality of the model, we also applied some training techniques.
	\item[--] We did experiments both on simulation and real robot system to demonstrate that the system can perform stably.
\end{itemize}

%----------------------------------------------------------------------------------------
\section{Structure of the document}

The outline of this thesis is first a background information and theory (see in chapter 2). Then the related work is presented in chapter 3. The proposed method for robot grasping detection and execution is presented in chapter 4. The experiment setup is described in chapter 5. Then, the detail experiment is presented in chapter 6. In chapter 7, we present the results and discussion. Finally, chapter 8 is the conclusion of the thesis.

%----------------------------------------------------------------------------------------

